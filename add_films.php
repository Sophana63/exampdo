<?php
require_once("class/MyPdo.php");
$pdo = new MyPDO(); 

$genres = $pdo -> reqFetchAll("SELECT * FROM genre"); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('partial/_head.php'); ?>
</head>

<body>
<!-- navbar -->
<?php include('partial/_navbar.php'); ?>

<hr>
<table class="titleTab">
  <tr>
    <td class='left'>
        <h1 class="userTitle"><i class="fa-solid fa-pen-to-square"></i> &nbsp;&nbsp; Ajouter un film</h1>
    </td>
    <td class='right'>        
        &nbsp;
    </td>
  </tr>
</table>    
<hr>

<div class="bodyClass">
<form class="row g-3" action="treat_add.php" method="post">
  <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">   
  <div class="col-md-6">
    <label for="title" class="form-label">Titre</label>
    <input type="text" class="form-control" id="title" name="title" >
  </div>
  <div class="col-md-6">
    <label for="inputDate" class="form-label">Date</label>
    <input type="date" class="form-control" id="inputDate" name="date">
  </div>
  <div class="input-group">
  <span class="input-group-text">Description</span>
    <textarea class="form-control" aria-label="With textarea" name="description"></textarea>
  </div>
  <select name="genre" class="form-select" aria-label="Default select example">    
    <?php foreach ($genres as $genre) { ?>
    <option value="<?php echo $genre->id; ?>"><?php echo $genre->name; ?></option>
    <?php } ?>
  </select>
  
  <div class="col-12">
    <button type="submit" class="btn btn-primary">Ajouter un film</button>
  </div>
</form>
</div>
    
</body>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</html>
