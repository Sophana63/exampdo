<?php 
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    require_once("class/MyPdo.php");
    $pdo = new MyPDO();
    $result = $pdo -> reqFetch("
        SELECT *, genre.name AS genreName FROM movie
        JOIN genre ON genre.id = movie.genre_id
        WHERE movie.id = " .$id."
    "); 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('partial/_head.php'); ?>
</head>

<body>
<!-- navbar -->
<?php include('partial/_navbar.php'); ?>


<hr>
<table class="titleTab">
  <tr>
    <td class='left'>
        <h1 class="userTitle"><i class="fa-solid fa-film"></i> &nbsp;&nbsp; Liste des Films</h1>
    </td>
    <td class='right'>        
        <div class="button_slide slide_right"><a href="add_films.php"><i class="fa-solid fa-plus"></i> &nbsp;Ajouter </a></div>
    </td>
  </tr>
</table>    
<hr>

<div class="bodyClass">
<div class="card" style="width: 25rem; margin: auto">
  <img src="assets/images/<?php echo $result['images']; ?>" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title"><?php echo $result['title']; ?></h5>
    <p class="card-text"><?php echo $result['description']; ?></p>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Parution: <?php $d = new DateTime($result['date']); echo $d->format('d-m-Y'); ?></li>
    <li class="list-group-item">Genre: <?php echo $result['genreName']; ?></li>
  </ul>
</div>
</div>

</body>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</html>