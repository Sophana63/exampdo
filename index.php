<?php 
require_once("class/MyPdo.php");
$pdo = new MyPDO();
$result = $pdo -> reqFetchAll("SELECT * FROM movie");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('partial/_head.php'); ?>
</head>

<body>
<!-- navbar -->
<?php include('partial/_navbar.php'); ?>


<hr>
<table class="titleTab">
  <tr>
    <td class='left'>
        <h1 class="userTitle"><i class="fa-solid fa-film"></i> &nbsp;&nbsp; Liste des Films</h1>
    </td>
    <td class='right'>        
        <div class="button_slide slide_right"><a href="add_films.php"><i class="fa-solid fa-plus"></i> &nbsp;Ajouter </a></div>
    </td>
  </tr>
</table>    
<hr>

<div class="bodyClass">
<div class="dataList">
    <div class="table-responsive">
        <table class="table table-striped table-dark">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Titre</th>
              <th scope="col">Date de parution</th>              
              <th scope="col"></th>              
              <th scope="col"></th>              
              <th scope="col"></th>              
            </tr>
          </thead>
          <tbody>
          <?php foreach ($result as $movie) { ?>
            <tr>
                <td><?php echo $movie->id; ?></td>
                <td><?php echo $movie->title; ?></td>
                <td><?php $d = new DateTime($movie->date); echo $d->format('d-m-Y');?></td>                
                <td><a href="film.php?id=<?php echo $movie->id; ?>"><i class="fa-solid fa-magnifying-glass"></i></a></td>
                <td><a href="modif_films.php?id=<?php echo $movie->id; ?>"><i class="fa-solid fa-pen-to-square"></i></a></td>
                <td><a href="del_film.php?id=<?php echo $movie->id; ?>"  onclick="return confirm('Supprimer le film?')"><i class="fa-solid fa-trash-can"></i></a></td>
            </tr>
            <?php } ?>     
          </tbody>
        </table>
        
    </div>
</div>
</div>

</body>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</html>