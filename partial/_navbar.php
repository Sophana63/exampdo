<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./../index.php">Films</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Series</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Animés</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled">Divers</a>
        </li>
      </ul>

      <form class="d-flex" method="get" action="./../search.php">
        <input class="form-control me-2" type="search" placeholder="un titre..." aria-label="Search" name="search" id="search">
        <button class="btn btn-outline-success" type="submit">Rechercher</button>
      </form>

    </div>
  </div>
</nav>