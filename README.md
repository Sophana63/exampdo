# examPDO

examen PDO avec Jules

Le fichier .sql se trouve dans le dossier /database, à injecter dans mySQL:
Elle permet :
 - création la base de données
 - création des tables 'movie' et 'genre'
 - insertion de données des tables ci-dessus

Pour exécuter le projet, lancer un serveur php:
```
php -S localhost:8000
```

## Fonction Projet

Le projet est une liste de films où on peut:
 - créer
 - modifier
 - supprimer
 - afficher
 - rechercher par titre

Une class PDO (dans le dossier /class/MyPDO.php) permet de simplifier les requêtes.

## Ajouter Projet à Git
```
cd existing_repo
git remote add origin https://gitlab.com/Sophana63/exampdo.git
git branch -M main
git push -uf origin main
php -S localhost:8000
```

